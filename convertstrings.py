# stdlib
import argparse
import json
import re

DEBUG=False
emptydict = {}

keyword_subst = re.compile("\[([a-z0-9_]+)\]")

def do_replace(s):
	r = []
	prev_pos = 0
	for m in keyword_subst.finditer(s):
		key = m.group(1)
		replacement = "{{ .%s }}" % key

		pos = m.start()
		r.append(s[prev_pos:pos])
		r.append(replacement)
		prev_pos = m.end()

	r.append(s[prev_pos:])
	return "".join(r)

def convert_recursively(j, key=(), newj=None):
	if newj is None: newj = []

	for k, v in j.items():
		if isinstance(v, dict):
			convert_recursively(v, key + (k,), newj)
		else:
			#print(".".join(key), k, v)
			#print(" >", do_replace(v))
			newj.append({"id": ".".join(key + (k,)), "translation": do_replace(v)})

	return newj

def convert_file(infilename, outfilename):
	with open(infilename, "r") as f:
		j = json.load(f)
		newj = convert_recursively(j)

		#print(json.dumps(newj, indent=1))

	with open(outfilename, "w") as f:
		f.write(json.dumps(newj, indent=1))

class ArgumentParser(argparse.ArgumentParser):
	def format_usage(self):
		s = "{}Try '{} --help' for more information.\n\n"
		return s.format(argparse.ArgumentParser.format_usage(self), self.prog)

def parse_args():
	parser = ArgumentParser(description="Convert Jekyll i18n strings file to Hugo i18n file")
	parser.add_argument("infile", help="Jekyll strings.json file.")
	parser.add_argument("outfile", help="Hugo i18n json file.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debugging.")
	options = parser.parse_args()
	return options

if __name__ == '__main__':
	options = parse_args()
	DEBUG = options.debug

	convert_file(options.infile, options.outfile)

